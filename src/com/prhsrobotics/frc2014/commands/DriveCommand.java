/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.commands;

import com.prhsrobotics.frc2014.Constants;
import com.prhsrobotics.frc2014.OI;

/**
 *
 * @author Rohil
 */
public class DriveCommand extends CommandBase {
    
    private double old_wheel = 0.0;
    private double quick_stop_accumulator;
    private double throttle_deadzone = 0.05;
    private double turn_deadzone = 0.05;
    
    public DriveCommand() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(drive);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        drive.disableControllers();
    }
    
    protected double handleDeadzone(double value, double deadzone){
        return (Math.abs(value) > Math.abs(deadzone)) ? value : 0.0;
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        boolean is_quick_turn = OI.getQuickTurn();
        double throttle = - handleDeadzone(OI.driver_joystick.getRawAxis(Constants.left_stick_y.getInt()), throttle_deadzone);
        double turn = handleDeadzone(OI.driver_joystick.getRawAxis(Constants.right_stick_x.getInt()), turn_deadzone);
        double negative_inertia = turn - old_wheel;
        old_wheel = turn;
        double wheel_nonlinearity = 0.6;
        turn = Math.sin(Math.PI/2.0 * wheel_nonlinearity * turn) / Math.sin(Math.PI/2.0 * wheel_nonlinearity);
        turn = Math.sin(Math.PI/2.0 * wheel_nonlinearity * turn) / Math.sin(Math.PI/2.0 * wheel_nonlinearity);
        double left_pwm, right_pwm;
        double sensitivity = .85;
        double angular_power;
        double linear_power;
        double negative_inertia_accumulator = 0.0;
        double negative_inertia_scalar = 5.0;
        double negative_inertia_power = negative_inertia * negative_inertia_scalar;
        double over_power;
        negative_inertia_accumulator += negative_inertia_power;
        turn = turn + negative_inertia_accumulator;
        if(negative_inertia_accumulator > 1){
            negative_inertia_accumulator -= 1;
        }else if(negative_inertia_accumulator < -1){
            negative_inertia_accumulator += 1;
        }else {
            negative_inertia_accumulator = 0;
        }
        linear_power = throttle;
        if(is_quick_turn){
            if(Math.abs(linear_power) < 0.2){
                double alpha = 0.1;
                quick_stop_accumulator = (1 - alpha) * quick_stop_accumulator + alpha * ((Math.abs(turn) < 1.0) ? turn : 1.0 * (turn < 0 ? -1 : 1)) * 5;
            }
            over_power = 1.0;
            angular_power = turn;
        } else {
            over_power = 0.0;
            angular_power = Math.abs(throttle) * turn * sensitivity - quick_stop_accumulator;
            if (quick_stop_accumulator > 1){
                quick_stop_accumulator -= 1;
            } else if(quick_stop_accumulator < -1){
                quick_stop_accumulator += 1;
            } else{
                quick_stop_accumulator = 0.0;
            }
        }
        angular_power = Math.abs(throttle) * turn * sensitivity - quick_stop_accumulator;
        if(quick_stop_accumulator>1){
            quick_stop_accumulator -= 1;
        }else if(quick_stop_accumulator < -1){
            quick_stop_accumulator += 1;
        }else {
            quick_stop_accumulator = 0.0;
        }
        right_pwm = left_pwm = linear_power;
        left_pwm += angular_power;
        right_pwm -= angular_power;
        if (left_pwm > 1.0) {
        right_pwm -= over_power * (left_pwm - 1.0);
        left_pwm = 1.0;
        } else if (right_pwm > 1.0) {
        left_pwm -= over_power * (right_pwm - 1.0);
        right_pwm = 1.0;
        } else if (left_pwm < -1.0) {
        right_pwm += over_power * (-1.0 - left_pwm);
        left_pwm = -1.0;
        } else if (right_pwm < -1.0) {
        left_pwm += over_power * (-1.0 - right_pwm);
        right_pwm = -1.0;
        }
        drive.setLeftRightPower(left_pwm, right_pwm);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
