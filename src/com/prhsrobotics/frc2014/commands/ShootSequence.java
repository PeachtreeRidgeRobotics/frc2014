/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;
import com.prhsrobotics.frc2014.commands.*;
import com.prhsrobotics.frc2014.Constants;
import edu.wpi.first.wpilibj.command.WaitCommand;

/**
 *
 * @author Rohil
 */
public class ShootSequence extends CommandGroup {
    
    public ShootSequence() {
        // Add Commands here:
        // e.g. addSequential(new Command1());
        //      addSequential(new Command2());
        // these will run in order.

        // To run multiple commands at the same time,
        // use addParallel()
        // e.g. addParallel(new Command1());
        //      addSequential(new Command2());
        // Command1 and Command2 will run in parallel.
        // A command group will require all of the subsystems that each member
        // would require.
        // e.g. if Command1 requires chassis, and Command2 requires arm,
        // a CommandGroup containing them would require both the chassis and the
        // arm.
        if(Constants.intake_position.getInt() == 0){
            addSequential(new IntakePush());
        }
        addSequential(new Shoot());
        addSequential(new WaitCommand(2));
        addSequential(new ResetShooter());
    }
}
