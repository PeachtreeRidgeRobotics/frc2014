/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.commands;

import com.prhsrobotics.frc2014.Constants;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author Rohil
 */
public class PushToSmartDashboard extends CommandBase {
    
    public PushToSmartDashboard() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(sensors);
        requires(choose_automation);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        SmartDashboard.putBoolean("Intake Light Sensory", sensors.getLightSensorIntakeState());
        SmartDashboard.putBoolean("Intake Held Sensory", sensors.getLightSensorHeldState());
        Constants.automated_state.setVal(choose_automation.getChooseAutomation());
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
