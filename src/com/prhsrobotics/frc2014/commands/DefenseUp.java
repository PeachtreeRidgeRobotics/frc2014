/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.commands;

/**
 *
 * @author Rohil
 */
public class DefenseUp extends CommandBase {
    
    public DefenseUp() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(defensive_arm);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        defensive_arm.defense_up();
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
        defensive_arm.defense_stop();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        defensive_arm.defense_stop();
    }
}
