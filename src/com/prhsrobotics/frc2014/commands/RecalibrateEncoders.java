/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prhsrobotics.frc2014.commands;

/**
 *
 * @author Rohil
 */
public class RecalibrateEncoders extends CommandBase{
    protected void initialize(){
        drive.resetEncoders();
    }
    
    protected void execute(){
    }
    
    protected boolean isFinished(){
        return true;
    }
    
    protected void interrupted(){
        
    }
    
    protected void end(){
        
    }
}
