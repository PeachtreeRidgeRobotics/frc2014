/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prhsrobotics.frc2014.control;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

/**
 *
 * @author Rohil
 */
public class ControlUpdater {
  private static ControlUpdater instance = null;
  private Vector systems = new Vector();
  private Timer control_updater;
  private double period = 1.0 / 100.0;

  private class UpdaterTask extends TimerTask {
    private ControlUpdater updater;

    public UpdaterTask(ControlUpdater updater) {
      if (updater == null) {
        throw new NullPointerException("Given ControlUpdater was null");
      }
      this.updater = updater;
    }

    public void run() {
      updater.update();
    }
  }

  public void update() {
    for(int i = 0; i < systems.size(); i++) {
      ((Updatable)systems.elementAt(i)).update();
    }
  }

  public static ControlUpdater getInstance() {
    if(instance == null) {
      instance = new ControlUpdater();
    }
    return instance;
  }

  public void start() {
    if(control_updater == null) {
      control_updater = new Timer();
      control_updater.schedule(new UpdaterTask(this), 0L, (long) (this.period * 1000));
    }
  }

  public void stop() {
    if(control_updater != null) {
      control_updater.cancel();
      control_updater = null;
    }
  }

  public void add(Updatable system) {
    systems.addElement(system);
  }

  public void setPeriod(double period) {
    this.period = period;
    stop();
    start();
  }
}
