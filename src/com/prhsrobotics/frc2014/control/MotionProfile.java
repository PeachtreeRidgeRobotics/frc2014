/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prhsrobotics.frc2014.control;

/**
 *
 * @author Rohil
 */
public interface MotionProfile {
    public double updateSetpoint (double current_setpoint, double current_source, double current_time);
    public double setGoal (double goal, double current_source, double t);
}
