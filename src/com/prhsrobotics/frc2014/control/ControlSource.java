/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prhsrobotics.frc2014.control;

/**
 *
 * @author Rohil
 */
public interface ControlSource {
    public double get();
    public void updateFilter();
    public boolean getLowerLimit();
    public boolean getUpperLimit();
}
