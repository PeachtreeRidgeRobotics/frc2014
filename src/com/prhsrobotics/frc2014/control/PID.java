/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prhsrobotics.frc2014.control;

import com.prhsrobotics.frc2014.lib.util.ConstantsBase.Constant;
/**
 *
 * @author RoboLions
 */
public class PID {
    Constant kP;
    Constant kI;
    Constant kD;
    Constant kF;
    
    public PID(Constant p, Constant i, Constant d, Constant f){
        set(p, i, d, f);
    }
    
    public PID(Constant p, Constant i, Constant d){
        set(p, i, d);
    }
    
    public final void set(Constant p, Constant i, Constant d, Constant f){
        Constant nullConstant = new Constant("nullPID","0");
        if(p != null){
            kP = p;
        }
        else{
            kP = nullConstant;
        }
        if(i != null){
            kI = i;
        }
        else{
            kI = nullConstant;
        }
        if(d != null){
            kD = d;
        }
        else{
            kD = nullConstant;
        }
        if(f != null){
            kF = f;
        }
        else{
            kF = nullConstant;
        }
    }
    
    public final void set(Constant p, Constant i, Constant d){
        set(p,i,d,new Constant("nullF", "0"));
    }
    
    public final void set(double p, double i, double d){
        kP.setVal(p);
        kI.setVal(i);
        kD.setVal(d);
    }
    
    public double getP(){
        return kP.getDouble();
    }
    
    public double getI(){
        return kI.getDouble();
    }
    
    public double getD(){
        return kD.getDouble();
    }
    
    public double getF(){
        return kF.getDouble();
    }
}
