/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.subsystems;

import com.prhsrobotics.frc2014.Constants;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 * @author Rohil
 */
public class Sensors extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    private DigitalInput lightSensorIntake = new DigitalInput(Constants.light_sensor_intake.getInt());
    private DigitalInput lightSensorHeld = new DigitalInput(Constants.light_sensor_held.getInt());
    
    public boolean getLightSensorIntakeState()
    {
        return lightSensorIntake.get();
    }
    
    public boolean getLightSensorHeldState()
    {
        return lightSensorHeld.get();
    }
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
