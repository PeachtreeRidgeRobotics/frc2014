/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.subsystems;

import com.prhsrobotics.frc2014.Constants;
import com.prhsrobotics.frc2014.commands.CompressorStart;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 * @author Rohil
 */
public class CompressorSystem extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    
    private Compressor compressor = new Compressor(Constants.compressor_pressure_switch.getInt(), Constants.compressor_port.getInt());
    
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        setDefaultCommand(new CompressorStart());
    }
    
    public void compressor_start(){
        compressor.start();
    }
}
