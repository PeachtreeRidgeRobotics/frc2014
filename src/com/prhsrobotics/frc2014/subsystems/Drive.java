/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prhsrobotics.frc2014.subsystems;

import com.prhsrobotics.frc2014.Constants;
import com.prhsrobotics.frc2014.commands.DriveCommand;
import com.prhsrobotics.frc2014.control.ControlSource;
import com.prhsrobotics.frc2014.lib.util.Gyro;
import edu.wpi.first.wpilibj.command.Subsystem;
import com.prhsrobotics.frc2014.lib.util.RelativeEncoder;
import com.prhsrobotics.frc2014.control.ControlOutput;
import com.prhsrobotics.frc2014.control.MotionProfile;
import com.prhsrobotics.frc2014.lib.profiles.TrapezoidalProfile;
import com.prhsrobotics.frc2014.lib.profiles.PIDProfile;
import com.prhsrobotics.frc2014.control.PID;

/**
 *
 * @author Rohil
 */
public class Drive extends Subsystem{

    private DriveTrain motors;
    RelativeEncoder left_encoder;
    RelativeEncoder right_encoder;
    
    TrapezoidalProfile profile = new TrapezoidalProfile(6*12.0,.25);
    PID straight_gains = new PID(Constants.straight_kP,Constants.straight_kI,Constants.straight_kD);
    PIDProfile straight_controller= new PIDProfile("straight_controller" , straight_gains, new DriveControlSource(true), new DriveControlOutput(true), profile);
    PID turn_gains = new PID(Constants.turn_kP,Constants.turn_kI,Constants.turn_kD);
    PIDProfile turn_controller = new PIDProfile("turn_controller", turn_gains, new DriveControlSource(false), new DriveControlOutput(false), profile);
    
    Gyro gyro = new Gyro(Constants.gyro_port.getInt());
    
    protected class DriveControlSource implements ControlSource {
        boolean straight = true;
        DriveControlSource(boolean straight) {
            this.straight = straight;
        }
        
        public double get(){
            if (straight) {
                return (getLeftEncoderDistance() + getRightEncoderDistance()) / 2.0;
            } else{
                return getGyroAngle();
            }
        }
        
        public void updateFilter(){
        }
        
        public boolean getLowerLimit(){
            return false;
        }
        
        public boolean getUpperLimit() {
            return false;
        }
    }
    
    double last_straight = 0;
    double last_turn = 0;
    
    protected class DriveControlOutput implements ControlOutput {
        boolean straight;
        DriveControlOutput(boolean straight){
            this.straight = straight;
        }
        
        public void set(double value){
            if(straight){
                last_straight = value;
            } else {
                last_turn = value;
            }
            setLeftRightPower(last_straight + last_turn, last_straight - last_turn);
        }
    }
    
    public Drive(DriveTrain motors){
        super();
        this.motors = motors;
        left_encoder = new RelativeEncoder(motors.getLeftEncoder());
        right_encoder = new RelativeEncoder(motors.getRightEncoder());
        left_encoder.start();
        right_encoder.start();
        disableControllers();
    }
    
    protected void initDefaultCommand() {
        setDefaultCommand(new DriveCommand());
    }
    
    public void setLeftRightPower(double left_power, double right_power){
        motors.driveSetPower(left_power, -right_power);
    }
    
    public double getLeftEncoderDistance(){
        return left_encoder.get() / 256.0 * 2 * Math.PI;
    }
    
    public double getRightEncoderDistance(){
        return right_encoder.get() / 256.0 * 2 * Math.PI;
    }
    
    public void resetEncoders() {
        motors.getLeftEncoder().reset();
        motors.getRightEncoder().reset();
        left_encoder.reset();
        right_encoder.reset();
    }
    
    public double getGyroAngle(){
        return gyro.getAngle();
    }
    
    int resets = 0;
    public void resetGyro(){
        System.out.println("Resetting gyro!" + resets++);
        gyro.reset();
    }
    
    public void disableControllers(){
        straight_controller.disable();
        turn_controller.disable();
        setLeftRightPower(0,0);
    }
    
    public void setSpeedGoal(double speed, double angle){
        profile.setMaxVelocity(Math.abs(speed));
        profile.setTimeToMaxV(.001);
        straight_controller.setGoal(speed < 0 ? -1000 : 1000);
        straight_controller.enable();
        turn_controller.setGoal(angle);
        turn_controller.enable();
    }
    
    public void updatePositionGoal(double distance){
        straight_controller.setRawGoal(distance);
    }
    
    public void setTurnGoal(double angle){
        straight_controller.disable();
        last_straight = 0;
        turn_controller.setGoal(angle);
        turn_controller.enable();
    }
    
    public void updateTurnGoal(double angle){
        turn_controller.setRawGoal(angle);
    }
    
    public boolean onTarget(){
        return straight_controller.onTarget() && turn_controller.onTarget();
    }
    
    public void resetControllers(){
        straight_controller.setProfile(profile);
    }
    
    public void setStraightProfile(MotionProfile profile){
        straight_controller.setProfile(profile);
    }
    
    public void reinitGyro(){
        gyro.initGyro();
    }
}
