/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.Solenoid;
import com.prhsrobotics.frc2014.Constants;
import edu.wpi.first.wpilibj.Talon;

/**
 *
 * @author Rohil
 */
public class Intake extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    private Solenoid intake_a = new Solenoid(Constants.intake_solenoid_a.getInt());
    private Solenoid intake_b = new Solenoid(Constants.intake_solenoid_b.getInt());
    private Talon intake_motor = new Talon(Constants.intake_motor_port.getInt());
    
    public void intakeBall(){
        intake_motor.set(1.0);
    }
    
    public void outakeBall(){
        intake_motor.set(-1.0);
    }
    
    public void stopIntakeMotor(){
        intake_motor.set(0.0);
    }

    public void pushOut(){
        intake_a.set(true);
    }
    
    public void pushStop(){
        intake_a.set(false);
    }
    
    public void pullIn(){
        intake_b.set(true);
    }
    
    public void pullStop(){
        intake_b.set(false);
    }
    
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
