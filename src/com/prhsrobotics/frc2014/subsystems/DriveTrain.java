
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.Talon;
import com.prhsrobotics.frc2014.*;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.RobotDrive;
/**
 *
 * @author Rohil
 */
public class DriveTrain extends Subsystem{
    // Motor Controllers
    private Talon front_left = new Talon(Constants.front_left_drive_motor.getInt());
    private Talon front_right = new Talon(Constants.front_right_drive_motor.getInt());
    private Talon back_left = new Talon(Constants.rear_left_drive_motor.getInt());
    private Talon back_right = new Talon(Constants.rear_right_drive_motor.getInt());
    
    //Sensors
    private Encoder left_encoder = new Encoder(Constants.left_encoder_port_a.getInt(), Constants.left_encoder_port_b.getInt(), true);
    private Encoder right_encoder = new Encoder(Constants.right_encoder_port_a.getInt(), Constants.right_encoder_port_b.getInt());
    
    private boolean zeroed = false;
    
    public Encoder getLeftEncoder(){
        return left_encoder;
    }
    
    public Encoder getRightEncoder(){
        return right_encoder;
    }
    
    public void driveSetPower(double left_power , double right_power){
        front_left.set(left_power);
        front_right.set(right_power);
        back_left.set(left_power);
        back_right.set(right_power);
    }
    
    public void driveSetAllPower(double all_power){
        front_left.set(all_power);
        front_right.set(-all_power);
        back_left.set(all_power);
        back_right.set(-all_power);
    }
    
    public void setFrontLeft(double power){
        front_left.set(power);
    }
    
    public void setBackLeft(double power){
        back_left.set(power);
    }
    
    public void setFrontRight(double power){
        front_right.set(power);
    }
    
    public void setRearRight(double power){
        back_right.set(power);
    }
    
    public double getLeftPower(){
        return front_left.get();
    }
    
    public double getRightPower(){
        return front_right.get();
    }
    
    public void brake()
    {
        
    }
    protected void initDefaultCommand()
    {
        
    }
}
