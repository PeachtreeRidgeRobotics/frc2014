/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;

/**
 *
 * @author Rohil
 */
public class ChooseAutomation extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    SendableChooser automatedManipulator = new SendableChooser();
    public ChooseAutomation(){
    automatedManipulator.addDefault("Automatic Manipulator", "0");
    automatedManipulator.addObject("Manual", "1");
    }
    public int getChooseAutomation(){
    int a = ((Integer)automatedManipulator.getSelected()).intValue();
    return a;
    }
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
