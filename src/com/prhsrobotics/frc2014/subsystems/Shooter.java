/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.subsystems;

import com.prhsrobotics.frc2014.Constants;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.Solenoid;

/**
 *
 * @author Rohil
 */
public class Shooter extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    
    private Solenoid left_solenoid_a = new Solenoid(Constants.left_solenoid_a.getInt());
    private Solenoid left_solenoid_b = new Solenoid(Constants.left_solenoid_b.getInt());
    private Solenoid right_solenoid_a = new Solenoid(Constants.right_solenoid_a.getInt());
    private Solenoid right_solenoid_b = new Solenoid(Constants.right_solenoid_b.getInt());
    
    public void shoot(){
        left_solenoid_a.set(true);
        right_solenoid_a.set(true);
    }
    
    public void shoot_off(){
        left_solenoid_a.set(false);
        right_solenoid_a.set(false);
    }
    
    public void reload(){
        left_solenoid_b.set(true);
        right_solenoid_b.set(true);
    }
    
    public void reload_off(){
        left_solenoid_b.set(false);
        right_solenoid_b.set(false);
    }
    
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
