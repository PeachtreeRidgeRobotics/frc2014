/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prhsrobotics.frc2014.lib.profiles;

import com.prhsrobotics.frc2014.control.ControlOutput;
import com.prhsrobotics.frc2014.control.ControlSource;
import com.prhsrobotics.frc2014.control.MotionProfile;
import com.prhsrobotics.frc2014.control.PID;
import edu.wpi.first.wpilibj.Timer;

/**
 *
 * @author RoboLions
 */
public class PIDProfile extends PIDController{
    private Timer timer = new Timer();
    double original_goal;
    double sign = 1;
    MotionProfile profile;
    
    public PIDProfile(String name, PID gains, ControlSource source, ControlOutput output, MotionProfile profile){
        super(name, gains, source, output);
        this.profile = profile;
    }
    
    public void update(){
        if(enabled) {
            double t = timer.get();
            super.setGoal(profile.updateSetpoint(getGoal(), source.get(), t));
        }
        
        super.update();
    }
    
    public void setGoal(double goal) {
        original_goal = goal;
        timer.reset();
        timer.start();
        profile.setGoal(goal, source.get(), timer.get());
        super.setGoal(source.get());
    }
    
    public boolean onTarget(){
        boolean done = !enabled || (Math.abs(original_goal - last_source) < on_target_error) && (Math.abs(last_delta_error) < on_target_delta_error);
        if (done){
            System.out.println(name + " DONE");
        }
        return done;
    }
    
    public void setProfile(MotionProfile profile) {
        this.profile = profile;
        setGoal(source.get());
    }
}
