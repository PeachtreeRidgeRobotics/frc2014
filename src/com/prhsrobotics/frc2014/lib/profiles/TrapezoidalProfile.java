/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prhsrobotics.frc2014.lib.profiles;

import com.prhsrobotics.frc2014.control.MotionProfile;

/**
 *
 * @author Rohil
 */
public class TrapezoidalProfile implements MotionProfile {
    double last_time = 0;
    double acceleration;
    double velocity;
    double time_from_max_velocity;
    double time_to_max_velocity;
    double sign;
    double time_total;
    
    public TrapezoidalProfile(double maxV, double time_to_max_v){
        setMaxVelocity(maxV);
        setTimeToMaxV(time_to_max_v);
    }
    
    public final void setMaxVelocity(double v){
        velocity = v;
    }
    
    public final void setTimeToMaxV(double time_to_max_v){
        acceleration = velocity / time_to_max_v;
    }
    
    public double updateSetpoint(double current_setpoint, double current_source, double current_time){
        double setpoint = current_setpoint;
        double period = current_time - last_time;
        if(current_time < time_to_max_velocity) {
            setpoint += ((acceleration * current_time) * period * sign);
        } else if (current_time < time_from_max_velocity){
            setpoint += (velocity * period * sign);
        } else if (current_time < time_total){
            double deceleration_time = current_time - time_from_max_velocity;
            double v = velocity + (-acceleration * deceleration_time);
            setpoint += (v * period * sign);
        }
        last_time = current_time;
        return setpoint;
    }
    
    public double setGoal(double goal, double current_source, double time){
        double setpoint = goal - current_source;
        sign = (setpoint < 0) ? -1.0 : 1.0;
        time_to_max_velocity = velocity/acceleration;
        double delta_pos_max_v = (sign * setpoint) - (time_to_max_velocity * velocity);
        double time_at_max_v = delta_pos_max_v / velocity;
        time_from_max_velocity = time_to_max_velocity + time_at_max_v;
        time_total = time_from_max_velocity + time_to_max_velocity;
        last_time = time;
        return current_source;
    }
}
