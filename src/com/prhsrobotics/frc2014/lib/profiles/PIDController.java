/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.lib.profiles;

import com.prhsrobotics.frc2014.control.ControlOutput;
import com.prhsrobotics.frc2014.control.ControlSource;
import com.prhsrobotics.frc2014.control.Controller;
import com.prhsrobotics.frc2014.control.PID;

/**
 *
 * @author Rohil
 */
public class PIDController extends Controller {

    PID gains;
    ControlSource source;
    ControlOutput output;
    double goal;
    double error_sum;
    double last_error;
    double last_delta_error;
    double last_source;
    double last_out;
    double min_ierror = 10.0;
    double on_target_error = 2.0;
    double on_target_delta_error = .05;

    public PIDController(String name, PID gains, ControlSource source, ControlOutput output) {
        super(name);
        this.gains = gains;
        this.source = source;
        this.output = output;
        enabled = true;
        error_sum = 0.0;
        last_error = 0.0;
    }

    public void setGains(PID gains) {
        this.gains = gains;
    }

    public void setEpsilons(double on_target_error, double on_target_delta_error) {
        this.on_target_error = on_target_error;
        this.on_target_delta_error = on_target_delta_error;
    }

    public void update() {
        last_source = source.get();
        double out = 0;
        double error = goal - last_source;
        double p = gains.getP() * error;
        if (Math.abs(error) < min_ierror) {
            error_sum += error;
        }
        double i = gains.getI() * error_sum;
        double delta_error = error_sum - last_error;
        double d = gains.getD() * delta_error;
        last_error = error;
        double ff = gains.getF() * goal;
        if (enabled) {
            out = ff + p + i + d;
            output.set(out);
            last_out = out;
        }
        last_delta_error = delta_error;
    }

    public void setGoal(double goal) {
        error_sum = 0;
        this.goal = goal;
        output.set(0);
    }

    public void setRawGoal(double goal) {
        this.goal = goal;
    }

    public double getGoal() {
        return this.goal;
    }

    void setErrorBounds(double on_target_error, double on_target_delta_error) {
        this.on_target_error = on_target_error;
        this.on_target_delta_error = on_target_delta_error;
    }

    void setMinI(double min_ierror) {
        this.min_ierror = min_ierror;
    }
}
