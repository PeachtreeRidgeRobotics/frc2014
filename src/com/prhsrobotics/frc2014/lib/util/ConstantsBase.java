/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.lib.util;

import com.sun.squawk.microedition.io.FileConnection;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Vector;
import javax.microedition.io.Connector;



/**
 *
 * @author Rohil
 */
public abstract class ConstantsBase{
    
    private static final Vector constants = new Vector();
    private static final String constants_file_path = "Constants.txt";
    
    public static void readConstantsFromFile(){
        DataInputStream constants_input_stream;
        FileConnection constants_file;
        byte[] buffer = new byte[255];
        String content = "";
        try{
             constants_file = (FileConnection)Connector.open("file:///" + constants_file_path , Connector.READ);
             constants_input_stream = constants_file.openDataInputStream();
             while(constants_input_stream.read(buffer) != -1){
                 content += new String(buffer);
             }
             constants_input_stream.close();
             constants_file.close();
             
         String[] lines = Util.split(content , "\n");
         for(int i = 0; i < constants.size() ; i++ ){
             String[] line = Util.split(lines[i], "=");
             if (line.length != 2) {
                 System.out.println("Error: invalid constants file line:" + (lines[i].length() ==0 ? "(empty line)" : lines[i]));
                 continue;
             }
         
         boolean found = false;
         for (int j = 0; j<constants.size(); j++){
             Constant constant = (Constant) constants.elementAt(j);
             if(constant.getName().compareTo(line[0]) == 0){
                 System.out.println("Setting " + constant.getName() + " to " + line[1]);
                 constant.setVal(line[1]);
                 found = true;
                 break;
             }
         }
         if (!found){
             System.out.println("Error: the constant doesn't exist: " + lines[i]);
         }
         }
        }
        catch(IOException e){
            System.out.println("Constants.txt not found. Not overriding constants.");
        }
        catch (Exception e){
           e.printStackTrace();
        }
    }


/**
 *
 * @author Rohil
 */
public static class Constant {
    private String name;
    private String value;
    
    public Constant(String name, String value){
        this.name = name;
        this.value = value;
        constants.addElement (this);
    }
    
    public String getName(){
        return name;
    }
    
    public double getDouble(){
        return Double.parseDouble(value);
    }
    
    public int getInt(){
        return Integer.parseInt(value);
    }
    
    public String getString(){
        return value;
    }
    
    public void setVal (double value){
        this.value = Double.toString(value);
    }
    
    public void setVal (int value){
        this.value = Integer.toString(value);
    }
    
    public void setVal(String value){
        this.value = value;
    }
    
    public String toString(){
        return name + ": " + value;
    }
}
}
