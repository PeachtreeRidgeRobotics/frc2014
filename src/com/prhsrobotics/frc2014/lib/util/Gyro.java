/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prhsrobotics.frc2014.lib.util;

import edu.wpi.first.wpilibj.*;
import edu.wpi.first.wpilibj.communication.UsageReporting;

/**
 *
 * @author Rohil
 */
public class Gyro extends SensorBase{
  static final int kOversampleBits = 10;
  static final int kAverageBits = 0;
  static final double kSamplesPerSecond = 50.0;
  static final double kCalibrationSampleTime = 5.0;
  static final double kDefaultVoltsPerDegreePerSecond = 0.007;
  AnalogChannel m_analog;
  double m_voltsPerDegreePerSecond;
  double m_offset;
  boolean m_channelAllocated;
  AccumulatorResult result;
  
  public void initGyro() {
    result = new AccumulatorResult();
    if (m_analog == null) {
      System.out.println("Null m_analog");
    }
    m_voltsPerDegreePerSecond = kDefaultVoltsPerDegreePerSecond;
    m_analog.setAverageBits(kAverageBits);
    m_analog.setOversampleBits(kOversampleBits);
    double sampleRate = kSamplesPerSecond * (1 << (kAverageBits + kOversampleBits));
    m_analog.getModule().setSampleRate(sampleRate);

    Timer.delay(1.0);
    m_analog.initAccumulator();

    Timer.delay(kCalibrationSampleTime);

    m_analog.getAccumulatorOutput(result);

    int center = (int) ((double)result.value / (double)result.count + .5);

    m_offset = ((double)result.value / (double)result.count) - (double)center;

    m_analog.setAccumulatorCenter(center);

    m_analog.setAccumulatorDeadband(0); ///< TODO: compute / parameterize this
    m_analog.resetAccumulator();

    UsageReporting.report(UsageReporting.kResourceType_Gyro, m_analog.getChannel(), m_analog.getModuleNumber()-1);
  }

  public Gyro(int slot, int channel) {
      m_analog = new AnalogChannel(slot,channel);
      m_channelAllocated = true;
      initGyro();
  }
  
  public Gyro(int channel){
      m_analog = new AnalogChannel(channel);
      m_channelAllocated = true;
      initGyro();
  }
  
  public Gyro(AnalogChannel channel){
      m_analog = channel;
      m_channelAllocated = false;
      initGyro();
  }
  
  public void reset() {
    if (m_analog != null) {
        m_analog.resetAccumulator();
    }
  }
  
  public void free() {
    if (m_analog != null && m_channelAllocated) {
        m_analog.free();
    }
    m_analog = null;
  }
  
  public double getAngle() {
    if (m_analog == null) {
      return 0.0;
    } else {
      m_analog.getAccumulatorOutput(result);

      long value = result.value - (long) (result.count * m_offset);

      double scaledValue = value * 1e-9 * m_analog.getLSBWeight() * (1 << m_analog.getAverageBits()) /
              (m_analog.getModule().getSampleRate() * m_voltsPerDegreePerSecond);

      return scaledValue;
    }
  }  
  
  public void setSensitivity(double voltsPerDegreePerSecond) {
    m_voltsPerDegreePerSecond = voltsPerDegreePerSecond;
  }
  
  public double pidGet() {
    return getAngle();
  }
  
}
