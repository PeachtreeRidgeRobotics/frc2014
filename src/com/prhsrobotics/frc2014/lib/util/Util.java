/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prhsrobotics.frc2014.lib.util;

import java.util.Vector;

/**
 *
 * @author Rohil
 */
public class Util {
    
    public static String[] split(String input, String delimitter){
        Vector node = new Vector();
        int index = input.indexOf(delimitter);
        while( index >= 0){
            node.addElement(input.substring(0 , index));
            input = input.substring(index + delimitter.length());
            index = input.indexOf(delimitter);
        }
        node.addElement(input);
        String[] return_string = new String[node.size()];
        for (int i = 0; i < node.size(); ++i){
            return_string[i] = (String) node.elementAt(i);
        }
        return return_string;
    }
}
