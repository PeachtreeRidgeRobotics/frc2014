/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prhsrobotics.frc2014.lib.util;
import edu.wpi.first.wpilibj.Encoder;

/**
 *
 * @author Rohil
 */
public class RelativeEncoder {
    
    private Encoder encoder;
    int val;
    
    public RelativeEncoder(Encoder e){
        this.encoder = e;
        val = 0;
    }
    
    public void reset(){
        val = encoder.get();
    }
    
    public void resetAbsolute(){
        encoder.reset();
        reset();
    }
    
    public int get(){
        return encoder.get() - val;
    }
    
    public void start(){
        encoder.start();
    }
}
